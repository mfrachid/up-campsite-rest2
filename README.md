# UP Campsite System
Basic Campsite project from UPGRADE

## Tecnologias

| Technology | Version|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Spring Boot`**|2.0.5.RELEASE|
|**`Apache Maven`**|3.3.9|
|**`MongoDB`**|v3.6.3-rc1|

## Premises

MongoDB must be running on for `development` and `test` environment.

## Builds

run in `modules`:
```bash
 mvn clean install
``` 
or
```
  mvn clean install -Pdev
```

## Run Application

run in `modules/rest`:
```
  mvn spring-boot:run
```

## Run Tests

run in `modules`:
```
  mvn clean install -Pfunctest
```

## Swagger

[http://{domain}:{port}/swagger-ui.html]
