package com.up;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.up.domain.Reservation;

/**
 * Application startup
 * 
 * @author Marcos Rachid
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.up")
@PropertySources(value = { @PropertySource("classpath:up/campsite.properties") })
public class CampsiteApplication {

	private static final Logger LOG = LoggerFactory.getLogger(CampsiteApplication.class);

	@Value("${application.admin.password:admin}")
	private String adminPassword;

	@Value("${application.min.reservation:1}")
	private Long minDayFromCurrentAbleToReserve; // 1 day default

	@Value("${application.max.reservation:30}")
	private Long maxDayFromCurrentAbleToReserve; // 1 month default

	@Autowired
	private MongoTemplate mongoTemplate;

	/**
	 * Main method, used to run the application.
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CampsiteApplication.class, args);

		LOG.debug("Let's inspect the beans provided by Spring Boot:");

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			LOG.debug(beanName);
		}
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		Locale locale = new Locale("en", "US");
		sessionLocaleResolver.setDefaultLocale(locale);
		return sessionLocaleResolver;
	}

	/**
	 * Every midnight a new Reservation day is opened
	 */
	@Scheduled(cron = "0 0 0 * * *", zone = "UTC")
	public synchronized void openNewReservationDate() {
		mongoTemplate.save(new Reservation(LocalDate.now().plusDays(maxDayFromCurrentAbleToReserve)), "reservation");
	}

	/**
	 * Admin creation
	 * 
	 * @param mongoTemplate
	 */
	@PostConstruct
	public void setup() {
		LocalDate startingAbleDay = LocalDate.now().plusDays(minDayFromCurrentAbleToReserve);
		LocalDate lastingAbleDay = LocalDate.now().plusDays(maxDayFromCurrentAbleToReserve);
		List<Reservation> dates = Stream.iterate(startingAbleDay, d -> d.plusDays(1))
				.limit(ChronoUnit.DAYS.between(startingAbleDay, lastingAbleDay) + 1).map(d -> new Reservation(d))
				.collect(Collectors.toList());
		List<Reservation> reservations = mongoTemplate.find(
				Query.query(Criteria.where("reservationDate").gte(startingAbleDay).lte(lastingAbleDay)),
				Reservation.class);
		dates.removeAll(reservations);
		mongoTemplate.insertAll(dates);
	}

}
