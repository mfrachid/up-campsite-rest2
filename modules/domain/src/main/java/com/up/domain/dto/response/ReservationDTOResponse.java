package com.up.domain.dto.response;

import java.io.Serializable;
import java.time.LocalDate;

public class ReservationDTOResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private LocalDate reservationDate;

	public ReservationDTOResponse() {
	}

	public ReservationDTOResponse(String id, LocalDate reservationDate) {
		this.id = id;
		this.reservationDate = reservationDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(LocalDate reservationDate) {
		this.reservationDate = reservationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((reservationDate == null) ? 0 : reservationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReservationDTOResponse other = (ReservationDTOResponse) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (reservationDate == null) {
			if (other.reservationDate != null)
				return false;
		} else if (!reservationDate.equals(other.reservationDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReservationDTOResponse [id=" + id + ", reservationDate=" + reservationDate + "]";
	}

}
