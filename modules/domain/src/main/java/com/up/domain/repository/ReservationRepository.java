package com.up.domain.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.up.domain.Reservation;

public interface ReservationRepository extends MongoRepository<Reservation, String> {

	List<Reservation> findByReservationDateBetween(LocalDate startDate, LocalDate endDate);

	Page<Reservation> findByEmailNullAndReservationDateBetween(LocalDate startDate, LocalDate endDate,
			Pageable pageable);

	List<Reservation> findByEmailAndReservationDateBetween(String email, LocalDate startDate, LocalDate endDate);

}
