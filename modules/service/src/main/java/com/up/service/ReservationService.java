package com.up.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.up.domain.dto.response.ReservationDTOResponse;
import com.up.service.exception.BusinessException;

public interface ReservationService {

	Page<LocalDate> getAvailability(LocalDate startDate, LocalDate endDate, Pageable pageable);

	List<ReservationDTOResponse> reserve(LocalDate startDate, LocalDate endDate, String email, String fullName) throws BusinessException;

	void cancelReservation(String id);

}
