package com.up.handler;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.up.domain.dto.request.ReservationDTORequest;
import com.up.domain.dto.response.ReservationDTOResponse;
import com.up.service.exception.BusinessException;

public interface ReservationHandler {

	Page<LocalDate> getAvailability(LocalDate startDate, LocalDate endDate, Pageable pageable) throws BusinessException;

	List<ReservationDTOResponse> reserve(ReservationDTORequest dto) throws BusinessException;

	void cancelReservation(String id) throws BusinessException;

}
