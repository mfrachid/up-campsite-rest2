package com.up.handler.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.up.domain.dto.request.ReservationDTORequest;
import com.up.domain.dto.response.ReservationDTOResponse;
import com.up.handler.ReservationHandler;
import com.up.service.ReservationService;
import com.up.service.exception.BusinessException;

@Component
@Transactional
public class ReservationHandlerImpl implements ReservationHandler {

	@Value("${application.min.reservation:1}")
	private Long minDayFromCurrentAbleToReserve; // 1 day default

	@Value("${application.max.reservation:30}")
	private Long maxDayFromCurrentAbleToReserve; // 1 month default

	private ReservationService reservationsService;

	public ReservationHandlerImpl(ReservationService reservationsService) {
		this.reservationsService = reservationsService;
	}

	@Override
	public Page<LocalDate> getAvailability(LocalDate startDate, LocalDate endDate, Pageable pageable)
			throws BusinessException {
		if (startDate == null || endDate == null) {
			LocalDate currentDate = LocalDate.now();
			return reservationsService.getAvailability(currentDate.plusDays(minDayFromCurrentAbleToReserve),
					currentDate.plusDays(maxDayFromCurrentAbleToReserve), pageable);
		} else {
			checkReservationDates(startDate, endDate);
			return reservationsService.getAvailability(startDate, endDate, pageable);
		}
	}

	@Override
	public List<ReservationDTOResponse> reserve(ReservationDTORequest dto) throws BusinessException {
		LocalDate startDate = null;
		LocalDate endDate = null;
		try {
			startDate = LocalDate.parse(dto.getStartDate());
			endDate = LocalDate.parse(dto.getEndDate());
		} catch (DateTimeParseException e) {
			throw new BusinessException("Date fields format must be yyyy-MM-dd");
		}
		checkReservationDates(startDate, endDate);
		return reservationsService.reserve(startDate, endDate, dto.getEmail(), dto.getFullName());
	}

	@Override
	public void cancelReservation(String id) throws BusinessException {
		reservationsService.cancelReservation(id);
	}

	/**
	 * Check if the reservation dates are between the available dates
	 * 
	 * @param startDate
	 * @param endDate
	 * @throws BusinessException
	 */
	private void checkReservationDates(LocalDate startDate, LocalDate endDate) throws BusinessException {
		if (endDate.isBefore(startDate))
			throw new BusinessException("The start date cannot be before end date");
		LocalDate startingAbleDay = LocalDate.now().plusDays(minDayFromCurrentAbleToReserve);
		LocalDate lastingAbleDay = LocalDate.now().plusDays(maxDayFromCurrentAbleToReserve);
		if (startDate.isBefore(startingAbleDay))
			throw new BusinessException(
					"The start date cannot be before " + startingAbleDay.format(DateTimeFormatter.ISO_DATE));
		if (endDate.isAfter(lastingAbleDay))
			throw new BusinessException(
					"The end date cannot be after " + lastingAbleDay.format(DateTimeFormatter.ISO_DATE));
	}

}
