package com.up.integration;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.up.CampsiteApplication;

/**
 * 
 * @author Marcos Rachid
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = { CampsiteApplication.class })
public abstract class CampsiteIntegrationTest {

	@Autowired
	protected RestTemplate restTemplate;

	@Autowired
	protected MongoTemplate mongoTemplate;

	@Autowired
	protected ObjectMapper objectMapper;

}
