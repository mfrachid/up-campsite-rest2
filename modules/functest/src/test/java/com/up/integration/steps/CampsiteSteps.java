package com.up.integration.steps;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.up.domain.dto.ResponseDTO;
import com.up.domain.dto.request.ReservationDTORequest;
import com.up.integration.CampsiteIntegrationTest;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * 
 * @author Marcos Rachid
 *
 */
public class CampsiteSteps extends CampsiteIntegrationTest {

	private static final Logger LOG = LoggerFactory.getLogger(CampsiteSteps.class);

	private Exception e;
	private Exception eUser1;
	private Exception eUser2;

	private ResponseDTO user1Response;
	private ResponseDTO user2Response;

	public static final String BASE_URL = "http://localhost:8080/api/v1/";
	public static final String RESERVATIONS_URL = BASE_URL + "/reservations";

	@After
	public void end() {
		mongoTemplate.dropCollection("reservation");
	}

	@When("Both users try to request same reservation")
	public void parallelRequests() {
		try {
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			Runnable run1 = () -> {
				LOG.debug("Starting thread from user1");
				try {
					ReservationDTORequest json = new ReservationDTORequest();
					json.setStartDate(LocalDate.now().plusDays(3).format(DateTimeFormatter.ISO_DATE));
					json.setEndDate(LocalDate.now().plusDays(4).format(DateTimeFormatter.ISO_DATE));
					json.setEmail("user1@user.com");
					json.setFullName("user1");
					HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(json), headers);
					this.user1Response = restTemplate.postForObject(RESERVATIONS_URL, request, ResponseDTO.class);
				} catch (Exception e) {
					this.eUser1 = e;
				}
				LOG.debug("Ending thread from user1");
			};
			Runnable run2 = () -> {
				LOG.debug("Starting thread from user2");
				try {
					ReservationDTORequest json = new ReservationDTORequest();
					json.setStartDate(LocalDate.now().plusDays(3).format(DateTimeFormatter.ISO_DATE));
					json.setEndDate(LocalDate.now().plusDays(4).format(DateTimeFormatter.ISO_DATE));
					json.setEmail("user2@user.com");
					json.setFullName("user2");
					HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(json), headers);
					this.user2Response = restTemplate.postForObject(RESERVATIONS_URL, request, ResponseDTO.class);
				} catch (Exception e) {
					this.eUser2 = e;
				}
				LOG.debug("Ending thread from user2");
			};
			ExecutorService executor = Executors.newFixedThreadPool(2);
			executor.submit(run1);
			executor.submit(run2);
			executor.awaitTermination(5, TimeUnit.SECONDS);
		} catch (Exception e) {
			this.e = e;
		}
	}

	@Then("Only one gets it throught")
	public void checkResults() {
		LOG.debug((user1Response != null) ? "user1 data: " + user1Response.getData()
				: "user1 exception: " + eUser1.getMessage());
		LOG.debug((user2Response != null) ? "user2 data: " + user2Response.getData()
				: "user2 exception: " + eUser2.getMessage());
		List user1List = (user1Response != null) ? (List) user1Response.getData() : new ArrayList<>();
		List user2List = (user2Response != null) ? (List) user2Response.getData() : new ArrayList<>();
		assertTrue((this.eUser2 != null && !user1List.isEmpty()) || (this.eUser1 != null && !user2List.isEmpty()));
		assertNull(e);
	}

}
